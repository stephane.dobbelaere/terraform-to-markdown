#!/usr/bin/python3
import re
import getopt
import sys

class Terramarkdown:

    def __init__(self):
        """"class constructor """

    def show_usage(self):
        """"Usage code """
        print(sys.argv[0] + " [Options]")
        print("Options:")
        print("-h, --help / Print usage")
        print("-o, --output / specify the output filename if not specified generate terra.md file")
        print("-i, --input / Specify the input (output of the command terraform show)")

    def parse_ressources(self, filename):
        '''parse output file of terraform show'''
        ressources = {}
        with open(filename) as origin_file:
            for line in origin_file:
                found = re.findall(r' arn = ', line)
                if found:
                    #print(line)
                    type = line.split(':')[2]
                    ressource = line.split(':')[5]
                    ressources.setdefault(type, []).append(ressource)
        return ressources

    def table_markdown(self, ressources, filename):
        ''' generate table markdown from return of parse_ressources'''
        f = open(filename, "w")
        f.write("## List of ressources used by the project\n")

        f.write("\n| Type | Ressources |")
        f.write("\n| --- | --- |")
        for type,elements in ressources.items():
            ressource = "<br>".join(elements)
            f.write("\n| {} | {} |".format(type, ressource.replace("\n","")))

def main(argv):
    terramarkdown = Terramarkdown()
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["help", "input=", "output="])
        if len(opts) == 0:
            terramarkdown.show_usage()
            sys.exit()
        for opt, arg in opts:
            #print("opt:" + opt)
            if opt in ("-h", "--help", ""):
                terramarkdown.show_usage()
                sys.exit()
            elif opt in ("-i", "--input"):
                input = arg

            elif opt in ("-o", "--output"):
                output = arg

        terramarkdown.table_markdown(terramarkdown.parse_ressources(input),"terra.md")

    except getopt.GetoptError:
        print("Bad usage!")
        terramarkdown.show_usage()
        sys.exit(2)

if __name__ == "__main__":
   main(sys.argv[1:])
