# terraform-to-markdown

Generate a markdown file with output of terraform show

# usage

./tera-doc.py [Options]

Options:

| Options | Description |
| --- | --- |
|-h, --help | Print usage|
| -o, --output | specify the output filename if not specified generate terra.md file |
| -i, --input | Specify the input (output of the command terraform show) |

# example result:

## List of ressources used by the project

| Type | Ressources |
| --- | --- |
| dynamodb | table/demo<br>table/demo<br>table/demo-equation |
| iam | policy/demo<br>policy/demo<br>policy/demo-api-demo-access<br>policy/demo-cognito-access<br>policy/demo-team-endpoint<br>policy/demo-lambda-compute<br>policy/demo-lambda-endpoint<br>role/demo-test<br>policy/demo-lambda-cicd<br>policy/demo-compute-lambda<br>role/demo-compute<br>policy/demo-lambda<br>role/demo<br>policy/demo-int-show-files-test-lambda<br>role/demo-int-show-files-test<br>policy/demo-s3-cicd<br>policy/demo-int-demo-s3_generated |
| cloudfront | distribution/456789456789 |
| s3 | demo-int-cdn-test-origin<br>demo-int-cdn-test-origin<br>demo-int-generated-test |
| kms | alias/demo<br>key/123456-132456-123456-1a8979f |
| lambda | function<br>function<br>function |
