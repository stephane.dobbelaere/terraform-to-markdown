## List of ressources used by the project

| Type | Ressources |
| --- | --- |
| dynamodb | table/demo<br>table/demo<br>table/demo-equation |
| iam | policy/demo<br>policy/demo<br>policy/demo-api-demo-access<br>policy/demo-cognito-access<br>policy/demo-team-endpoint<br>policy/demo-lambda-compute<br>policy/demo-lambda-endpoint<br>role/demo-test<br>policy/demo-lambda-cicd<br>policy/demo-compute-lambda<br>role/demo-compute<br>policy/demo-lambda<br>role/demo<br>policy/demo-int-show-files-test-lambda<br>role/demo-int-show-files-test<br>policy/demo-s3-cicd<br>policy/demo-int-demo-s3_generated |
| cloudfront | distribution/E3R8H7F8WT4UI4 |
| s3 | demo-int-cdn-test-origin<br>demo-int-cdn-test-origin<br>demo-int-generated-test |
| kms | alias/demo<br>key/14920ae0-f30b-49b1-8685-a5f801a8979f |
| lambda | function<br>function<br>function |